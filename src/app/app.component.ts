import { Component } from '@angular/core';

import { UserResponse } from '../interfaces/phonespecs';
import { GetPhoneSpecs } from '../services/getPhoneSpecs';
import { ProcessAllPhones } from '../services/processAllPhones';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  phone: UserResponse|null = null;

  technologies: Array<string>;
  buttons_2g: Array<string>;
  buttons_3g: Array<string>;
  buttons_4g: Array<string>;

  constructor(
    private getPhoneSpecs: GetPhoneSpecs,
    private processAllPhones: ProcessAllPhones) {
  }

  getOnePlus5() {
    this.getPhoneSpecs.requestPhoneSpecs('OnePlus', '5', '1').subscribe(data => {
      this.phone = data;
      this.processAllPhones.add(data);
      });
    }

  getAppleX() {
    this.getPhoneSpecs.requestPhoneSpecs('Apple', 'X', '1').subscribe(data => {
      this.phone = data;
      this.processAllPhones.add(data);
      });
    }

  getPixel2() {
    this.getPhoneSpecs.requestPhoneSpecs('Google', 'Pixel 2', '1').subscribe(data => {
      this.phone = data;
      this.processAllPhones.add(data);
      });
  }

  getIPhone8() {
    this.getPhoneSpecs.requestPhoneSpecs('Apple', 'iPhone 8', '1').subscribe(data => {
      this.phone = data;
      this.processAllPhones.add(data);
      });
    }

  getSamsungGalaxyS9() {
    this.getPhoneSpecs.requestPhoneSpecs('Samsung', 'Galaxy S9', '1').subscribe(data => {
      this.phone = data;
      this.processAllPhones.add(data);
      });
    }

  getSamsungGalaxyS8() {
    this.getPhoneSpecs.requestPhoneSpecs('Samsung', 'Galaxy S8', '1').subscribe(data => {
      this.phone = data;
      this.processAllPhones.add(data);
      });
    }
    
  getHuaweiP20() {
    this.getPhoneSpecs.requestPhoneSpecs('Huawei', 'P20', '1').subscribe(data => {
      this.phone = data;
      this.processAllPhones.add(data);
      });
    }
  
  getLGG7() {
    this.getPhoneSpecs.requestPhoneSpecs('LG', 'LG G7 ', '1').subscribe(data => {
      this.phone = data;
      this.processAllPhones.add(data);
      });
    }

  getLGV30() {
    this.getPhoneSpecs.requestPhoneSpecs('LG', 'LG V30', '1').subscribe(data => {
      this.phone = data;
      this.processAllPhones.add(data);
      });
    }
  
  getHuaweiMate10() {
    this.getPhoneSpecs.requestPhoneSpecs('Huawei', 'Mate 10', '1').subscribe(data => {
      this.phone = data;
      this.processAllPhones.add(data);
      });
    }
    
  getSamsungGalaxyNote8() {
    this.getPhoneSpecs.requestPhoneSpecs('Samsung', 'Galaxy Note 8', '1').subscribe(data => {
      this.phone = data;
      this.processAllPhones.add(data);
      });
    }

  total() {
    this.processAllPhones.stringify();
  }

  get2g() {
    this.buttons_2g = this.processAllPhones.get2G();
  }

  get3g() {
    this.buttons_3g = this.processAllPhones.get3G();
  }

  get4g() {
    this.buttons_4g = this.processAllPhones.get4G();
  }

  getTechnologies() {
    this.technologies = this.processAllPhones.getTechnologies();
  }

}
