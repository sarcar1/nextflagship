import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { GetPhoneSpecs } from "../services/getPhoneSpecs";
import { ProcessAllPhones } from '../services/processAllPhones';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    GetPhoneSpecs,
    ProcessAllPhones
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
