import { Injectable } from '@angular/core';
import { UserResponse } from "../interfaces/phonespecs";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class GetPhoneSpecs {
    data: UserResponse|null = null;
    private token: string = 'cb99b4d22c983401a19e6d795fe5d0c83a35c3e415a7b201';

    constructor(private http: HttpClient) {
    }

    requestPhoneSpecs(brand: string, device: string, limit: string) {

        return this.http.get<UserResponse>(
            'https://fonoapi.freshpixl.com/v1/getdevice?' +
            'token=' + this.token + 
            '&limit=' + limit + 
            '&device=' + device + 
            '&brand=' + brand)
        .map((res:UserResponse) => {
            return res;
        });
    }
    
}