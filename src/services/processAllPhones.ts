import { Injectable } from '@angular/core';
import { UserResponse } from '../interfaces/phonespecs';
import { _ } from 'underscore';

@Injectable()
export class ProcessAllPhones {
    allPhones: Array<UserResponse> = [];

    _2g: Array<string> = [];
    _3g: Array<string> = [];
    _4g: Array<string> = [];
    technology: Array<string> = [];

    constructor() {
    }

    add(data: any) {
        this.allPhones = this.allPhones.concat(data);
        console.log('this.allPhones: ', this.allPhones);
    }

    stringify() {
        console.log(JSON.stringify(this.allPhones));
    }

    getTechnologies(): Array<string> {
        for(let phone of this.allPhones) {
            console.log(phone.DeviceName, ' has: ', phone.technology);
            // String() solves undefined issue on phone._4g_bands
            let phone_technology_array = String(phone.technology).split(',');
            this.technology = _.union(this.technology, phone_technology_array);
        }
        console.log('technology: ',this.technology);
        return this.technology;
        
    }


    get2G(): Array<string> {
        for(let phone of this.allPhones) {
            console.log(phone.DeviceName, ' has: ', phone._2g_bands);
            // String() solves undefined issue on phone._2g_bands
            let phone_2g_bands_array = String(phone._2g_bands).split(',');
            this._2g = _.union(this._2g, phone_2g_bands_array);
        }
        console.log('2g: ',this._2g);
        return this._2g;
        
    }

    get3G(): Array<string> {
        for(let phone of this.allPhones) {
            console.log(phone.DeviceName, ' has: ', phone._3g_bands);
            // String() solves undefined issue on phone._3g_bands
            let phone_3g_bands_array = String(phone._3g_bands).split(',');
            this._3g = _.union(this._3g, phone_3g_bands_array);
        }
        console.log('3g: ',this._3g);
        return this._3g;
        
    }

    get4G(): Array<string> {
        for(let phone of this.allPhones) {
            console.log(phone.DeviceName, ' has: ', phone._4g_bands);
            // String() solves undefined issue on phone._4g_bands
            let phone_4g_bands_array = String(phone._4g_bands).split(',');
            this._4g = _.union(this._4g, phone_4g_bands_array);
        }
        console.log('4g: ',this._4g);
        return this._4g;
        
    }

}